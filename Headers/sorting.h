#ifndef SORTING_H
#define SORTING_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <math.h>
#include <string.h> /* memset */

struct status{

    /* Algortihm use: insertionSort(0), insertionSortBin(1), shellSort(2),
        bubbleSort(3), quickSort(4), mergeSort(5), heapSort(6)    
    */
    int algorithm;
    /* Array type: Sorted(0), Random(1), Inverse(2)*/
    int arrayType;
    /* Array lenght*/
    long unsigned int arrayLenght;
    /* Changes quantity realized byt the algortihm*/
    long long unsigned int swapNumber;
    /* Comparesions quantity realized byt the algortihm*/
    long long unsigned int compareNumber;
    /* Time (ms) spend in the sorting*/
    long unsigned int timeElapsed;    
    /* Interruption flag*/
    int interrupted;
    /* PRIVATE VARIABLE, DON'T USE!!!*/
    long unsigned int _clockTime;
};

struct inputFile{
    int32_t randomNumber; 
};

typedef struct status status_t;
typedef struct inputFile inputFile_t;

// MACROS
#ifndef MAX_TIME 
    #define MAX_TIME 10*60 // seconds
#endif

#define MAX_FUNCTIONS 7
#define FUNCTION_POINTERS &insertionSort, &insertionSortBin, &shellSort, &bubbleSort, &quickSort, &mergeSort, &heapSort
#define FUNCTION_STR "insertionSort", "insertionSortBin", "shellSort", "bubbleSort", "quickSort", "mergeSort", "heapSort"

#define MAX_ARRAYTYPES 3
#define ARRAYTYPE_STR "ordenado", "randomico", "inverso"

// UTILS.C
/* Copy 'len' elements from 'source' vector to 'dest' vector, using the 
    'firstIndex' as the inicial element to copy. */
void vecCopy(int *source, int *dest, int len, int firstIndex);
/* Copy 'len' elements from 'source' vector to 'dest' vector, using the 
    'firstIndex' as the inicial element to copy. */
void vecInv(int *source, int *dest, int len, int firstIndex);
/* Test if 'len' elementes of a vector 'v1' are equal another vector 'v2' 
    Return 1 if they are or Return 0 if not*/
int vecTest(int *v1, int *v2, int len);
/* Test if a vector 'v' with 'len' elements is sorted*/
int sortedVec(int *v, int len);
/* Write the vector 'v' with lenght 'len' in standard output. */
void vecPrint(int *v, int len);
/* Write the status struct 'stats' in standard output. */
void statusPrint(status_t stats); 

// INSERTIONSORTS.C
/* Sort 'len' elements of a vector 'v' using Insertion algorithm. Performance 
    of the algorithm are save in a status struct */
status_t insertionSort(int *v, int len);
/* Sort 'len' elements of a vector 'v' using Binary Insertion algortihm. 
    Performance of the algorithm are save in a status struct */
status_t insertionSortBin(int *v, int len);
/* Sort 'len' elements of a vector 'v' using Shell Sort algortihm. 
    Performance of the algorithm are save in a status struct */
status_t shellSort(int *v, int len);


// SWAPSORTS.C
/* Sort 'len' elements of a vector 'v' using Buble Sort algorithm. Performance 
    of the algorithm are save in a status struct */
status_t bubbleSort(int *v, int len);
/* Sort 'len' elements of a vector 'v' using Quick Sort algortihm. 
    Performance of the algorithm are save in a status struct */
status_t quickSort(int *v, int len);
/* Auxiliary function to compute recursively Quick Sort Algorithm. */
void qSort(int *v, int low, int high, status_t *stats);
/* Auxiliary function to create partitions that Quick Sort Algoritjm needs. */
int part(int *v, int low, int high, status_t *stats);

// SELECITONSORTS.C 
/* Sort 'len' elements of a vector 'v' using Selection Sort algorithm. Performance 
    of the algorithm are save in a status struct */
status_t selectionSort(int *v, int len);
/* Sort 'len' elements of a vector 'v' using Heap Sort algortihm. 
    Performance of the algorithm are save in a status struct */
status_t heapSort(int *v, int len);
/* Auxiliary function to ensure the heap property. */
void heapfy(int *v, int len, int index, status_t *stats);
/* Auxiliary function to create a heap tree through a some vector 'v'. */
void buildHeap(int *v, int len, status_t *stats);

// INTERCALATIONSORTS.C 
/* Sort 'len' elements of a vector 'v' using Merge Sort algorithm. Performance 
    of the algorithm are save in a status struct */
status_t mergeSort(int *v, int len);
/* Vector v:
 ----------------------------------------------------------------------------
 ** v[0] ** v[1] ** v[2] ** v[3] ** v[4] ** v[...] ** v[len-2] ** v[len-1] **
 ----------------------------------------------------------------------------
 ___ p ____________________________ q-1 _____ q  ___________________ r-1 ____
*/
/* Auxiliary function that are recursely called to split the vector 'v'. */
void mSort(int *v, int p, int r, status_t *stats);
/* Auxiliary function to sort elements that was be prevously. */
void merge(int *v, int p, int q, int r, status_t *stats);


#endif

