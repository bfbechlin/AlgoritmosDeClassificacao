#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "sorting.h"

#define MODE 0 // operation mode 
#define MAX_NUMBERS 10000000 // major numbers of elements that will be tested
#define ITERATIONS 7 // how many tests by algorithm will be done 

int main(int argc, char *argv[]){
    // DATA STRUCTS
    inputFile_t data;
    status_t info;
    // FILES
    FILE *input, *output;
    // INDEX AND AUXILIARY VARIABLES
    int i, j, k, l, lenght;
    int *srcNumbers[3];
    int *toSort;
    // TIME VARIABLE
    unsigned long clockT;
    // VECTOR OF FUNCTIONS POINTERS
    status_t (*func[MAX_FUNCTIONS])(int*, int) = {FUNCTION_POINTERS};
    // VECTOR OF STATIC STRINGS
    char *algorithmStr[MAX_FUNCTIONS] = {FUNCTION_STR};
    char *arrayTypeStr[MAX_ARRAYTYPES] = {ARRAYTYPE_STR}; 
    // MATRIX OF INTERRUPTED ALGORITHMS
    int interruptStatus[ITERATIONS][MAX_FUNCTIONS][MAX_ARRAYTYPES];

    // ALLOCATING MEMORY
    input = fopen("randomnumbers.bin", "rb");
    output = fopen("resultados_00230321.txt", "w");
    for(i = 0; i < 3; i++)
         srcNumbers[i] = (int*)malloc(sizeof(int)*MAX_NUMBERS);
    toSort = (int*)malloc(sizeof(int)*MAX_NUMBERS);
    
    // READING THE INPUT FILE AND ORGANIZING SOURCES VECTORS 
    //  WHERE:
    //      srcNumbers[0] = SORTED;
    //      srcNumbers[1] = RANDOM;
    //      srcNumbers[2] = INVERSE;
    for(i = 0; i < MAX_NUMBERS; i++){
        fread(&data, sizeof(inputFile_t), 1, input);
        srcNumbers[1][i] = data.randomNumber;
    }
    vecCopy(srcNumbers[1], srcNumbers[0], MAX_NUMBERS, 0);
    func[5](srcNumbers[0], MAX_NUMBERS);
    vecInv(srcNumbers[0], srcNumbers[2], MAX_NUMBERS, 0);

    printf("Tempo máximo para execução de cada algoritmo será %i segundos.\n", MAX_TIME);
    // MAIN PROCESSING 
    // ------------------------------------------------------------------------
    // COMPUTING THE ALGORITHMS AND PRITING THE RESULTS INTO OUTPUT FILE
    // BE CAREFULL WITH ARRAY LENGHTS, BECAUSE THE MOST PART OF COMPLEXITIES 
    // ARE QUADRATIC. 
    lenght = MAX_NUMBERS;
    for(i = 0; i < ITERATIONS; i++){
        lenght = lenght/10;
        for(j = 0; j < MAX_FUNCTIONS; j ++){
            for(k = 0; k < MAX_ARRAYTYPES; k++){
                interruptStatus[i][j][k] = 0;
            }
        }    
    }        
    clockT = clock();
    for(i = 0; i < ITERATIONS; i++){
        lenght *= 10;
        for(j = 0; j < MAX_FUNCTIONS; j++){
            for(k = 0; k < 3; k++){
                printf("%s, array %s com %i elementos: ", algorithmStr[j], 
                        arrayTypeStr[k], lenght);

                if(!interruptStatus[i][j][k]){
                    info.arrayType = k;
                    vecCopy(srcNumbers[k], toSort, lenght, 0);
                    info = func[j](toSort, lenght);

                    if(info.interrupted){
                        for(l = i; l < ITERATIONS; l++){
                            interruptStatus[l][j][k] = 1;
                        }
                        printf("ESTOURO DE TEMPO MÁXIMO DE EXECUÇÃO (%i s).\n", 
                            MAX_TIME);
#if MODE == 0
                        fprintf(output, "%s, %s, %lu, ESTOURO DE TEMPO MÁXIMO "
                            "DE EXECUÇÃO (%i s).\n", algorithmStr[j], 
                            arrayTypeStr[k], info.arrayLenght, MAX_TIME);
#elif MODE == 1
                        fprintf(output, "*\n");
#endif
                    }

                    else{
                         printf("OK.\n");
                        fprintf(output, "%s, %s, %lu, %llu, %llu, %lu.\n", 
                        algorithmStr[j],arrayTypeStr[k], info.arrayLenght, 
                        info.compareNumber, info.swapNumber, info.timeElapsed);
                    }
                }

                else{
                    printf("ESTOURO DE TEMPO MÁXIMO DE EXECUÇÃO (%i s).\n", 
                        MAX_TIME);
#if MODE == 0
                    fprintf(output, "%s, %s, %lu, ESTOURO DE TEMPO MÁXIMO "
                        "DE EXECUÇÃO (%i s).\n", algorithmStr[j], 
                        arrayTypeStr[k], info.arrayLenght, MAX_TIME);
#elif MODE == 1
                    fprintf(output, "*\n");
#endif
                }
                fflush(output);
            }
        }    
    }
    clockT = (clock() - clockT)*1.0/CLOCKS_PER_SEC;
    printf("Tempo gasto para realizar toda a rotina de %i números e %i "
        "iterações foi de %lu segundos.\n", MAX_NUMBERS, ITERATIONS, clockT);

    // FREE MEMORY ALLOCATED                                                                   
    fclose(input);
    fclose(output);
    for(i = 0; i < 3; i++)
        free(srcNumbers[i]);
    free(toSort);   
    
    return 0;
}   
