#include "sorting.h"

status_t bubbleSort(int *v, int len){
    unsigned long long cmp = 0, swp = 0; 
    status_t stats;
    int i, j, aux;

    stats._clockTime = clock();
    for(i = len - 1; i > 0; i--){
        for(j = 0; j < i; j++){
            if(v[j] > v[j+1]){
                aux = v[j];
                v[j] = v[j+1];
                v[j+1] = aux;
                swp ++;
            }
            cmp++;
        }
		/* TIME EXCECPTION */
		if((clock() - stats._clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
            stats.interrupted = 1;
            break;
        }
    }
    stats.timeElapsed = (clock() - stats._clockTime)*1000.0/CLOCKS_PER_SEC;
    stats.arrayLenght = len;                             
    stats.swapNumber = swp;                              
    stats.compareNumber = cmp;                                              

    return stats;
}

status_t quickSort(int *v, int len){
    status_t stats;

    stats.compareNumber = 0;
    stats.swapNumber = 0;

    stats._clockTime = clock();
    qSort(v, 0, len - 1, &stats);
    
    stats.timeElapsed = (clock() - stats._clockTime)*1000.0/CLOCKS_PER_SEC;
    stats.arrayLenght = len; 

    return stats;
}

void qSort(int *v, int low, int high, status_t *stats){
    int pivot;
    if(high > low){
        pivot = part(v, low, high, stats);
		if(!stats->interrupted){
			qSort(v, low, pivot - 1, stats);
			/* TIME EXCECPTION */
			if((clock() - stats->_clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
	        	stats->interrupted = 1;
				return;
	    	}       
	        qSort(v, pivot + 1, high, stats);
		}
	}
}

int part(int *v, int low, int high, status_t *stats){
    int pivot = low;
    int i = pivot + 1, j = high, aux;
	int vp = v[pivot];
    while(j >= i){
		stats->compareNumber++;        
		if(v[i] <= vp)
            i++;

		else{
			stats->compareNumber++;        
			if(v[j] > vp)
     	       j--; 

			else{
            aux = v[i];
            v[i] = v[j];
            v[j] = aux;
			stats->swapNumber++;    
    		}
		}
		/* TIME EXCECPTION */
		if((clock() - stats->_clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
        	stats->interrupted = 1;
			return 0;
    	}                 
	}

	v[pivot] = v[j];
	v[j] = vp;
    stats->swapNumber++;
 
    return j;
}

