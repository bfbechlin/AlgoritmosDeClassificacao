#include "sorting.h"

status_t selectionSort(int *v, int len){
    unsigned long clockTime;
    unsigned long long cmp = 0, swp = 0;
    status_t stats;
    int i, j, min;
 
    stats.interrupted = 0;
    clockTime = clock();
    for(i = 0; i < len-1; i++){
        min = i;
        for(j = i+1; j < len; j++){
            if(v[j] < v[min]){
                min = j;
            }
            cmp ++;
        }
        if(i != min){
            j = v[i];
            v[i] = v[min];
            v[min] = j;
            
            swp++;
        }
		/* TIME EXCECPTION */
        if((clock()- clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
            stats.interrupted = 1;
            break;
        }
    } 
    clockTime = (clock() - clockTime)*1000.0/CLOCKS_PER_SEC;
    stats.arrayLenght = len;
    stats.swapNumber = swp;
    stats.compareNumber = cmp;
    stats.timeElapsed = clockTime;

    return stats;
}

status_t heapSort(int *v, int len){
    unsigned long clockTime;
    status_t stats;
    int i, aux;
 
    stats.swapNumber = 0;
    stats.compareNumber = 0;
    stats.interrupted = 0;    
    clockTime = clock();
    buildHeap(v, len, &stats);
    for(i = len -1; i > 0; i--){
        aux = v[i];
        v[i] = v[0];
        v[0] = aux;
        heapfy(v, i, 0, &stats);
		/* TIME EXCECPTION */
		if((clock()- clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
            stats.interrupted = 1;
            break;
        }
    }

    clockTime = (clock() - clockTime)*1000.0/CLOCKS_PER_SEC;
    stats.arrayLenght = len;
    stats.timeElapsed = clockTime;

    return stats;
}

void heapfy(int *v, int len, int index, status_t *stats){
    int left, right, bigger, aux;

    right = 2*(index+1);
    left = right - 1;
    bigger = index;
    if(left <  len){
        if(v[left] > v[bigger])
            bigger = left;
        stats->compareNumber++;
    }
    if(right <  len){
        if(v[right] > v[bigger])
            bigger = right;
        stats->compareNumber++;
    }
    if(bigger != index){
        aux = v[bigger];
        v[bigger] = v[index];
        v[index] = aux;
        stats->swapNumber++;
        heapfy(v, len, bigger, stats);
    }

}

void buildHeap(int *v, int len, status_t *stats){
    int i;
    for(i = len/2 -1; i >= 0; i--)
        heapfy(v, len, i, stats);
    
}
