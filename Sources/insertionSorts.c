#include "sorting.h"


status_t insertionSort(int *v, int len){
    unsigned long long cmp = 0, swp = 0;
    status_t stats;
    int i, j, key;

	stats.interrupted = 0;
    stats._clockTime = clock();
    for(j = 1; j < len; j++){
        key = v[j];
        i = j - 1;
        while(i >= 0 && v[i] > key){            
            v[i + 1] = v[i];
            i --;

            cmp ++;
            swp ++;
        }
        v[i + 1] = key;
        cmp ++;
        /* swp ++; */

		/* TIME EXCECPTION */
		if((clock() - stats._clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
            stats.interrupted = 1;
            break;
        }
    }
    stats.timeElapsed = (clock() - stats._clockTime)*1000.0/CLOCKS_PER_SEC;
    stats.arrayLenght = len;
    stats.swapNumber = swp;
    stats.compareNumber = cmp;

    return stats;
}

status_t insertionSortBin(int *v, int len){
    unsigned long long cmp = 0, swp = 0;
    status_t stats;
    int j, i, k, key, mean;
    int below, over;

	stats.interrupted = 0;
    stats._clockTime = clock();
    for(j = 1; j < len; j++){
        
        /* Binary Search */
        below = 0;
        over = j;
        while(below < over){
            mean = (below + over)/2;
            if(v[j] > v[mean])
                below = mean + 1;
            else
                over = mean;
            cmp ++;
        }
        key = v[j];
        i = below;
        /* SWAPS */
        if(i != j){
            for(k = 0; k <= j - i - 1; k++){
                v[j - k] = v[j - k - 1];
                swp ++;
            }
            v[i] = key;
            /* swp ++;        */        
		}
		/* TIME EXCECPTION */
        if((clock() - stats._clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
            stats.interrupted = 1;
            break;
        }
    }
    stats.timeElapsed = (clock() - stats._clockTime)*1000.0/CLOCKS_PER_SEC;
    stats.arrayLenght = len;
    stats.swapNumber = swp;
    stats.compareNumber = cmp;

    return stats;
}

                                                         
status_t shellSort(int *v, int len){                          
    unsigned long long cmp = 0, swp = 0;
    status_t stats;                                        
    int h, i, j, k, key;                                 
    
    stats.interrupted = 0;
    stats._clockTime = clock();                                 
    /* PRAT SEQUENCY*/
    h = log2(len);                                       
    h = pow(2, h) - 1;                                   
    while(h > 0){                                        
        for(k = 0; k < h; k++){ 

            /* INSERTION SORT */
            for(j = h + k; j < len; j+= h){              
                key = v[j];                              
                i = j - h;                               
                while(i >= 0 && v[i] > key){              
                    v[i + h] = v[i];                     
                    i -= h;                              
                    cmp ++;                              
                    swp ++;                              
                }                                        
                v[i + h] = key;
                cmp ++;
                /* swp ++;                                   */
            }
			/* TIME EXCECPTION */
			if((clock() - stats._clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
            	stats.interrupted = 1;
            	break;
        	}                                            
        }   
		if(stats.interrupted == 1)
			break;                                             
        h = h/2;                                         
    }                                                    
    stats.timeElapsed = (clock() - stats._clockTime)*1000.0/CLOCKS_PER_SEC;
    stats.arrayLenght = len;                             
    stats.swapNumber = swp;                              
    stats.compareNumber = cmp;                                            

    return stats;
}                                                          
