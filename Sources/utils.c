#include "sorting.h"

void vecCopy(int *source, int *dest, int len, int firstIndex){
    memcpy((void*) dest, (void*) &source[firstIndex], len*sizeof(int));
    /* int i, lastIndex;                       */
    /* lastIndex = firstIndex + len;           */
    /* for(i = firstIndex; i < lastIndex; i++) */
    /*     dest[i] = source[i];                */
}

void vecInv(int *source, int *dest, int len, int firstIndex){
    int i, j, lastIndex;
    lastIndex = firstIndex + len - 1;
    j = 0;
    for(i = lastIndex; i >= firstIndex; i--)
        dest[j++] = source[i];
}

int testVec(int *v1, int *v2, int len){
    int i;
    for(i = 0; i < len; i++){
        if(v1[i] != v2[i])
            return 0;
    }    
    return 1;
}

int sortedVec(int *v, int len){
    int i; 
    for(i = 0; i < len-1; i++){
        if(v[i] > v[i+1])
            return 0;
    }
    return 1;

}

void vecPrint(int *v, int len){
    int i;
    printf("-----VECTOR-----\n");
    for(i = 0; i < len; i++){
        printf("%i ", v[i]);
    }
    printf("\n");
}

void statusPrint(status_t stats){
    printf(" Algortihm info:\nArray Type: ");
    switch(stats.arrayType){
        case 1:
            printf("Sorted\n");
            break;
        case 2:
            printf("Random\n");
            break;
        case 3:
            printf("Inverse\n");
            break;
        default:
            printf("Undefined\n");
            break;
    }
    printf("Array Lenght : %lu\nNumber of Swaps: %llu\nNumber of comparations: %llu\nTime spend (ms): %lu\n", stats.arrayLenght, stats.swapNumber, stats.compareNumber, stats.timeElapsed);
}

