#include "sorting.h"

status_t mergeSort(int *v, int len){
    status_t stats;
    
    stats.compareNumber = 0;
    stats.swapNumber = 0;
	stats.interrupted = 0;

    stats._clockTime = clock();
    mSort(v, 0, len, &stats);
    stats.timeElapsed = (clock() - stats._clockTime)*1000.0/CLOCKS_PER_SEC;
    stats.arrayLenght = len;                                                                              

    return stats;
}
void mSort(int *v, int p, int r, status_t *stats){
    int q = (p + r)/2;
    if(r - p > 1){
		if(!stats->interrupted){
			mSort(v, p, q, stats);
        	mSort(v, q, r, stats);
        	merge(v, p, q, r, stats);
		}
		/* TIME EXCECPTION */
		if((clock() - stats->_clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
        	stats->interrupted = 1;
			return;
    	}         
    }
}

void merge(int *v, int p, int q, int r, status_t *stats){
    int i = p, j = q, k = 0, len = r - p;
    int *aux = (int*) malloc((len)*sizeof(int));

	/* TIME EXCECPTION */
	if((clock() - stats->_clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
        stats->interrupted = 1;
		free(aux);
        return;
    }    
	while(i < q && j < r){
        if(v[i] <= v[j])
            aux[k++] = v[i++];
        else
            aux[k++] = v[j++];
        stats->compareNumber++;
        stats->swapNumber++;        
    }
	/* TIME EXCECPTION */
	if((clock() - stats->_clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
        stats->interrupted = 1;
		free(aux);
        return;
    }    
    while(i < q){
        aux[k++] = v[i++];
        stats->compareNumber++;
    }
	/* TIME EXCECPTION */
	if((clock() - stats->_clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
        stats->interrupted = 1;
		free(aux);
        return;
    }    
    while(j < r){
        aux[k++] = v[j++];
        stats->compareNumber++;
    }
	/* TIME EXCECPTION */
	if((clock() - stats->_clockTime)*1.0/CLOCKS_PER_SEC > MAX_TIME){
        stats->interrupted = 1;
		free(aux);
        return;
    }
    
    memcpy((void*) &v[p], (void*) aux, len*sizeof(int));
    free(aux);
}
