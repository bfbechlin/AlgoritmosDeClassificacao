# Maplex Default Makefile
# 	Modify the value of SRCS files and executable file MAIN.
# 	If you need more paths to include files only enter this in an array
#   separeted by spaces. OBS.: -I : currentPath
# 	SRCS don't NEED -I !!!

# define the C source files
SRCS = ./Sources/main.c ./Sources/insertionSorts.c ./Sources/swapSorts.c ./Sources/selectionSorts.c ./Sources/intercalationSorts.c ./Sources/utils.c  

# define the executable file
MAIN = sorts

# define the C compiler to use
CC = gcc

# define any compile-time flags
CFLAGS = -Wall -g

# define any directories containing header files other than /usr/include
INCLUDES = -I./Headers

# define library paths in addition to /usr/lib
LFLAGS = -lm

# define any libraries to link into executable:
LIBS =

# define the C object files
OBJS = $(SRCS:.c=.o)


.PHONY: depend clean

all: msg $(MAIN) cleanObj

msg:
	@echo "Creating the executable and deleting the .o(obj) files."

$(MAIN): $(OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(LFLAGS) $(LIBS)

# this is a suffix replacement rule for building .o's from .c's
%.o:%.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@ $(LFLAGS)

cleanObj:
	$(RM) $(OBJS)

clean:
	$(RM) *.o *~ $(MAIN)

depend: $(SRCS)
	makedepend $(INCLUDES) $^

# --
