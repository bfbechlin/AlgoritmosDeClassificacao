# Algoritmos de ordenação

**Autor:** Béuren Bechlin

__INF01124 - Classificação e Pesquisa de Dados__ 

Prof. Leandro Krug Wives  DATA: 01/05/2016

Projeto disponível em GPL v3

---

## Introdução

Existem vários algoritmos para classificação de dados e esses algoritmos possuem
compormentos distintos. Logo para encontrar a melhor soluçãopara cada caso 
específico é necessário compará-los e estudar sua complexidade e comportantes
para os diversos tipos de entrada específicos. Nesse contexto foi proposto que
desenvolvessemos os principais algoritmos de ordenação de dados.

Foram eles:

* Inserção Direta
* Inserção Direta com busca Binária
* Shellsort
* Bubblesort
* Quicksort
* Heapsort
* Mergesort

Também nos foi proposto que observassemos algumas variáveis importantes que
ajudam a definir o comportamento e complexidade de um algoritmo. 

Foram elas:
* Tempo de execução
* Quantidade de dados avaliados
* Tipo dos dados de entrada 
    * array ordenado
    * array randômico
    * array inversamente ordenado
* Quantidade de trocas
* Quantidade de comparações

Lembrando que os algoritmos aqui desenvolvidos não possuem grandes alterações do
algoritmo padrão, ou seja, não possuem otimizações. A maior parte dos algoritmos
que estão aqui desenvolvidos são baseados no material disponibilizado pelo
professor _Leandro Krug Wives_.

---

## Usando o projeto

Esse projeto foi desenvolvido em linguagem C. Para compilar o projeto foi usado 
o compilador GCC e __somente__ as bibliotecas padrões que estão sempre 
disponíveis com qualquer compilador para essa linguagem.

Para compilar e linkar o programa com as bibliotecas necessárias foi desenvolvido
um [Makefile](https://www.gnu.org/software/make/manual/make.html#Overview). Caso
deseje utilizá-lo va para a pasta raiz do projeto e digite o seguinte ocmando:


``` Unix
make
```

Então será gerado o executável _sorts_. Para executar somenter:
``` Unix
./sorts
```
__Soluções em sistemas baseados e Unix__

---

## Main.c

Esse fonte foi desenvolvido específicamente para o trabalho proposto, então está
muito modularizado. Alguns tópicos importantes caso queira reutilizar.

### Constantes
* MODE: pode ser atrubuido 0 ou 1. Utilizado para alterar a forma com que escreve
no arquivo de saída quando ocorre um estouro de tempo(será explicado adiante).
* MAX_NUMBER: o maior número de elementos de um array que será testado.
* ITERATIONS: a quantidade iterações que serão realizadas, ou seja, é a cada 
iteração a quantidade de elementos a ser testada é dividida por 10.

``` c
MODE X  
ITERATIONS Y
MAX_NUMBER Z
```

### Entrada e saida

São feitas por meio de um arquivo de entrada e outro de saída. Sendo que a entrada
usa um arquivo binário só com número em 32 bits.
``` c
input = fopen("randomnumbers.bin", "rb");
output = fopen("resultados_00230321.txt", "w");
```

### Cuidados importantes

Cuidado com a quantidade de número a ser testados já que algumas complexidades de
pior caso são _n²_ então sua execução pode demorar muito. Além disso, pode ocorrer
estouro de pilha, nos testes locais não foi observado, caso o número de elementos
seja muito grande, já que alguns algoritmos são recursivos e usando a pilha do 
sistema.

---

## Sorting.h

A maior parte da documentação consta no próprio arquivo, leia ele também se possível.

### Constantes

* MAX_TIME: *constante importante* tempo máximo que se espera para que um algoritmo
ordene o conjunto de dados enviados a ele
* MAX_FUNCTIONS: número de funções que foram implementadas. Selectionsort ficou
fora da contagem
* FUNCTION_POINTERS: label para ponteiros das funções que foram implementadas.
* FUNCTION_STR: nome das funções implementadas.
* MAX_ARRAYTYPES: número de tipos de array
* ARRAYTYPE_STR: nome dos tipos de array

### Como usar os funções de ordenação

Para usar as funções é necessário enviar o ponteiro do vetor de número inteiros
a ser ordenado e a quantidade de elementos. As funções retornam o comportamento
do algoritmo através de uma estrutura, caso não esteja interessado simplesmente
use como se fosse sem retorno.

A estrutura é a seguinte :
``` c
struct status{

/* Algortihm use: insertionSort(0), insertionSortBin(1), shellSort(2),
    bubbleSort(3), quickSort(4), mergeSort(5), heapSort(6)    
*/
int algorithm;
/* Array type: Sorted(0), Random(1), Inverse(2)*/
int arrayType;
/* Array lenght*/
long unsigned int arrayLenght;
/* Changes quantity realized byt the algortihm*/
long long unsigned int swapNumber;
/* Comparesions quantity realized byt the algortihm*/
long long unsigned int compareNumber;
/* Time (ms) spend in the sorting*/
long unsigned int timeElapsed;    
/* Interruption flag*/
int interrupted;
/* PRIVATE VARIABLE, DON'T USE!!!*/
long unsigned int _clockTime;
    
};
typedef struct status status_t;
```

### Utils.c
Nesse próprio header está descrito funções que auxiliam a depurar os algoritmos
como também visualizar de forma alternativa os resultados. Para mais informações
leia o próprio header _sorting.h_ onde está comentado _UTILS.C_.